# README #

### What is this repository for? ###

* This repository is for a small update notifier which will email any updates to the administrator. 
This is best used for headless servers, although could be used for any Arch Linux install, if you don't
want an update notifier tray icon.

### How do I get set up? ###

* Check that it's executable (chmod a+x ./check_update.sh) and run as root, with the email you wish to send to as the first argument.
You can also run this as a cronjob -- you'll only be notified if there is an update, all output from a cron is directed to /dev/null
(unless there is an error). 

### Who do I talk to? ###

* Aubrey Thomas <artxch@msn.com>