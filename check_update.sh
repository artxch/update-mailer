#!/usr/bin/bash

if [ "$UID" -ne 0 ]
then
	printf "Must run as root.\n"
	exit
fi

if [ -z $1 ]
then
	printf "Usage is: `basename $0` <email@host.tld>.\n"
	exit
fi

tty -s

if [ "$?" -eq 0 ]
then
	pacman -Sy
else
	pacman -Sy > /dev/null
fi
pacman -Qu > .updates && [ -s .updates ] && \
cat .updates | mail -s "Updates Available" $1 ; \
rm .updates
